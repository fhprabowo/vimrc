" ---------------------------------------------------------------------
" Initialization
" ---------------------------------------------------------------------

execute pathogen#infect()
syntax on                      " Enable syntax highlighting
filetype on                    " Enable filetype detection
filetype indent on             " Enable filetype-specific indenting
filetype plugin on             " Enable filetype-specific plugins
set encoding=utf-8             " Set encoding to utf-8
set guifont=DejaVu\ Sans\ Mono\ Bold\ 10
set background=dark
colorscheme solarized
let g:solarized_contrast = "high"
call togglebg#map("<F5>")


" ---------------------------------------------------------------------
" General Setting
" ---------------------------------------------------------------------

" Indentation
" =====================================================================
set expandtab                 " Convert tabs into spaces
set tabstop=4                 " Number of visual spaces per TAB
set softtabstop=4             " Number of spaces in tab when editing
set shiftwidth=4              " How many column for tab width
set autoindent                " Auto indentation

" Interface
" =====================================================================
set cursorline                " Highlight current line
set number                    " Show line number
set showcmd                   " Show command in bottom bar
set noshowmode                " Get rid default mode indicator
set visualbell                " No irritating sound
set showmatch                 " Show animation bracket animation
set matchtime=1               " Set duration match bracket animation
set guioptions+=r             " Toggle Right scroll bar
set guioptions+=L             " Toggle Left scroll bar
set guioptions+=T             " Toggle Toolbar
set guioptions+=m             " Toggle menu bar

" Searching
" =====================================================================
set ignorecase                " Ignoring case sensitivity
set incsearch                 " Search as characters are entered
set hlsearch                  " Hightlight all matches

" Folding
" =====================================================================
set foldmethod=indent         " Fold based on indent
set foldnestmax=10            " Deepest fold is 10 levels
set nofoldenable              " Dont fold by default
set foldlevel=1               " This is just what I use

" Misc
" =====================================================================
set noswapfile                " No Swap File
set nobackup                  " No Backup
set nowritebackup             " No Write Backup
set autochdir                 " Change to working dir of current file automatically
set wildmenu                  " Allow command mode to autocomplete path using tab like BASH
set switchbuf+=usetab,newtab  " When choosing a file from a quickfix buffer, open in a new tab or in an already opened tab
set timeout                   " Timeout on
set ttimeout                  " Ttimeout on
set timeoutlen=3000           " Time out on mapping after 3 seconds
set ttimeoutlen=100           " Time out on key codes after 0.1 seconds


" ---------------------------------------------------------------------
" Key Mapping
" ---------------------------------------------------------------------

let mapleader=","             " Leader key

" Edit vimrc
nmap <leader>ev :e ~/.vimrc<cr>

" INSERT mode back to NORMAL mode
imap jk <esc>

" Open explore
nmap <leader>ex :Explore<cr>

" Close window
nmap <leader>c :close<cr>

" CLose all windows but one
nmap <leader>1 :only<cr>

" Close buffer and move to the previous one
nmap <leader>x :bprevious <bar> bdelete #<cr>

" Next buffer
nmap <leader>l :bnext<cr>

" Prev buffer
nmap <leader>h :bprevious<cr>

" Save current buffer
nmap <leader>s :write<cr>

" Save all buffer
nmap <leader>sa :wall<cr>

" Quit
nmap <leader>q :q<cr>

